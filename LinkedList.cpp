#include <iostream>
using namespace std;

struct Node {
    int value;
    Node * next;
};

class LinkedList {
private:
    Node * head;
    int length;
public:
    LinkedList(void) {
        head = NULL;
        length = 0;
    }

    void insert(Node * newNode, int position) {
        if (length == 0) {
            head = newNode;
            length++;
            return;
        }
        if ((position < 0) || (position > length + 1)) {
            return;
        }
        if (position == 0) {
            newNode->next = head;
            head = newNode;
            length++;
        } else {
            Node * curr = head;
            for (int i = 0; i < position-1; i++) {
                curr = curr->next;
            }
            newNode->next = curr->next;
            curr->next = newNode;
            length++;
            return;
        }
    }

    void remove(int position) {
        if (length == 0) {
            return;
        }
        if ((position < 0 || position > length + 1)) {
            return;
        }
        if (position == 0) {
            head = head->next;
            length--;
        } else {
            Node * curr = head;
            Node * removed;
            for (int i = 0; i < position - 1; i++) {
                curr = curr->next;
            }
            removed = curr->next;
            curr->next = removed->next;
            length--;
            return;
        }
    }

    int search(int num) {
        int count = 0;
        Node * curr = head;
        while (count < length) {
            if (curr->value == num) {
                return count;
            } else {
                curr = curr->next;
                count++;
            }
        }
        return -1;
    }

    void print(void) {
        Node * curr = head;
        for (int i = 0; i < length; i++) {
            cout << curr->value << " ";
            curr = curr->next;
        }
        cout << endl;
        return;
    }
};

int main(void) {
    LinkedList test;
    for (int i = 5; i > 0; i--) {
        Node * temp = new Node;
        temp->value = i;
        test.insert(temp, 0);
    }
    test.remove(test.search(3));
    test.print();
    return 0;
}
